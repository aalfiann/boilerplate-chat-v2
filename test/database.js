/* global describe it DBMS */

'use strict';

require('make-promises-safe');
require('total.js');
const fs = require('fs');
const assert = require('assert');

describe('database test', function () {
  function readConfig () {
    try {
      const data = fs.readFileSync('config', 'utf8');
      const temp1 = data.split('\n');
      const temp2 = [];
      for (let i = 0; i < temp1.length; i++) {
        if (temp1[i].trim() !== '') {
          if (temp1[i].trim().substring(0, 2) !== '//') {
            temp2.push(temp1[i].trim());
          }
        }
      }
      const newdata = {};
      let arrData;
      for (let i = 0; i < temp2.length; i++) {
        arrData = temp2[i].split(':');
        if (arrData[1]) {
          if (arrData[0].trim() === 'postgre_conn') {
            newdata[arrData[0].trim()] = arrData[1].trim() + ':' + arrData[2] + ':' + arrData[3] + ':' + arrData[4];
          } else {
            newdata[arrData[0].trim()] = arrData[1].trim();
          }
        }
      }
      return newdata.postgre_conn;
    } catch (err) {
      return false;
    }
  }

  function isArray (a) {
    return (!!a) && (a.constructor === Array);
  }

  it('check configuration', async function () {
    let postgreConn = readConfig();
    if (postgreConn) {
      postgreConn = postgreConn.substring(0, 11);
    }
    assert.strictEqual(postgreConn, 'postgres://');
  });

  it('database connection', async function () {
    this.timeout(130000);
    require('dbms').init(readConfig());
    const result = await DBMS().query('select * from chat_message limit 1').promise();
    if (result) {
      assert.strictEqual(isArray(result), true);
    } else {
      assert.ok(false);
    }
  });
});
