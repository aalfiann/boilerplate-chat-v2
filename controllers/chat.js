/* global ROUTE */

'use strict';

exports.install = function () {
  // Routes
  ROUTE('/chat/{room_id}/{account_id}/{account_type_id}', room);
};

function room (roomid, accountid, accounttype) {
  const self = this;
  self.view('room', { room_id: roomid, account_id: accountid, account_type_id: accounttype });
}

// function viewChatroom (chatId, userId) {
//   const self = this;
//   const options = {};

//   options.chat_id = chatId;
//   options.user_id = userId;
//   self.view('index', { chatroom: chatId, userid: userId });
// }
