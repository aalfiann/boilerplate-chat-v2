/* global ROUTE */

'use strict';

exports.install = function () {
  ROUTE('/test/middleware', ['get', '*Test_route --> @passed', '#authorize']);
  ROUTE('/test/schema', ['post', '*Test_route --> @render', '#authorize']);
};
