/* global CORS */

'use strict';

exports.install = function () {
  // Sets cors for the entire API
  CORS('/*', ['get', 'post', 'put', 'delete', 'Access-Control-Allow-Origin: *'], true);
};
