/* global CONF ERROR */
'use strict';

require('dbms').init(CONF.postgre_conn, ERROR('PostgreSQL'));
