/* global F */

'use strict';

const FlyJson = require('fly-json-odm');
const helper = require(F.path.definitions('helper'));

const nosql = new FlyJson();

/**
 * Add user online in memory database
 * @param {array} mb            memory database in array
 * @param {string} socketId     socket id
 * @return {json}
 */
function addUser (mb, socketId) {
  socketId = (socketId === undefined) ? '' : socketId;
  const dt = { account_id: '', room_id: '', socket_id: socketId, timestamp: helper.timestamp(new Date()) };
  mb.push(dt);
  return {
    account_id: dt.account_id,
    room_id: dt.room_id,
    socket_id: dt.socket_id,
    timestamp: dt.timestamp,
    status: 'online'
  };
}

/**
 * Update user room
 * @param {array} mb            memory database
 * @param {string} socketId     socket id
 * @param {string} accountId    account_id
 * @param {string} roomId       room_id
 * @return {mixed}              json or false
 */
function updateUser (mb, socketId, accountId, roomId) {
  const result = nosql.set(mb)
    .where('socket_id', '===', socketId)
    .exec();
  if (result.length > 0) {
    const tmstamp = helper.timestamp(new Date());
    const mdf = nosql.setMode('shallow').set(mb)
      .modify('socket_id', socketId, { account_id: accountId, room_id: roomId, timestamp: tmstamp })
      .exec();
    mb = nosql.shallowClone(mdf);
    return {
      account_id: result[0].account_id,
      room_id: result[0].room_id,
      socket_id: result[0].socket_id,
      timestamp: tmstamp,
      status: 'online'
    };
  }
  return false;
}

/**
 * Get user data by socket id
 * @param {array} mb            memory database
 * @param {string} socketId     socket id
 * @return {array}
 */
function getUserBySocket (mb, socketId) {
  const result = nosql.setMode('shallow').set(mb)
    .where('socket_id', '===', socketId)
    .take(1)
    .exec();
  if (result.length > 0) {
    return result;
  }
  return [];
}

/**
 * Delete user in memory database by socket
 * @param {array} mb            memory database
 * @param {string} socketId     socket id
 * @return {bool}
 */
function leave (mb, socketId) {
  const deluser = nosql.setMode('shallow').set(mb)
    .delete('socket_id', socketId)
    .exec();
  mb = nosql.shallowClone(deluser);
}

/**
 * Logout User
 * @param {array} mb            memory database
 * @param {string} socketId     socket id
 * @return {array}
 */
function logoutUser (mb, socketId) {
  const result = false;
  const userInfo = getUserBySocket(mb, socketId);
  if (userInfo.length > 0 && userInfo[0] !== undefined) {
    leave(mb, socketId);
    userInfo[0].status = 'offline';
    return userInfo[0];
  }
  return result;
}

/**
 * Check is user online
 * @param {array} mb                memory database
 * @param {string} accountId        account_id
 * @param {string} roomId           room_id
 * @return {mixed}                  json / false
 */
function isUserOnline (mb, accountId, roomId) {
  roomId = (roomId === undefined) ? '' : roomId;
  nosql.setMode('shallow').set(mb)
    .where('account_id', '===', accountId);
  if (roomId) {
    nosql.where('room_id', '===', roomId);
  }
  const result = nosql.exec();
  if (result.length > 0) {
    return {
      account_id: result[0].account_id,
      room_id: ((roomId) ? result[0].room_id : ''),
      socket_id: result[0].socket_id,
      timestamp: result[0].timestamp,
      status: 'online'
    };
  }
  return false;
}

/**
 * Get user in spesific room
 * @param {array} mb            memory database
 * @param {string} roomId       room_id
 * @return {array}
 */
function listUserInRoom (mb, roomId) {
  return nosql.setMode('shallow').set(mb)
    .where('room_id', '==', roomId)
    .groupBy('account_id')
    .exec();
}

module.exports = {
  addUser,
  updateUser,
  getUserBySocket,
  leave,
  logoutUser,
  listUserInRoom,
  isUserOnline
};
