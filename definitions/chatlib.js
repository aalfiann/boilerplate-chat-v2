/* global F DBMS */
'use strict';

const helper = require(F.path.definitions('helper'));
const schema = require(F.path.schemas('message'));
const FlyJson = require('fly-json-odm');

/**
  * Add group date
  * @param {array} datahistory
  * @return {array}
  */
function _addGroupDate (datahistory) {
  const nosql = new FlyJson();
  const data = {
    posts: datahistory
  };
  const map = {
    list: 'posts',
    item: {
      room_id: 'room_id',
      account_id: 'account_id',
      account_type_id: 'account_type_id',
      message_type_id: 'message_type_id',
      message: 'message',
      message_id: 'message_id',
      date_created: 'date_created',
      date_created_unix: 'date_created_unix',
      message_status_id: 'message_status_id',
      status_active_id: 'status_active_id',
      date_modified: 'date_modified',
      date_modified_unix: 'date_modified_unix'
    },
    each: function (item) {
      item.date_created = helper.formatDate(item.date_created, true);
      item.date_created_unix = parseInt(item.date_created_unix);
      if (item.date_modified) {
        item.date_modified = helper.formatDate(item.date_modified, true);
        item.date_modified_unix = parseInt(item.date_modified_unix);
      }
      item.group_date = helper.formatDate(item.date_created);
      return item;
    }
  };
  return nosql.jsonTransform(data, map).make();
}

/**
 * Manipulation history
 * @param {array} data
 * @return {array}
 */
function _manipulationHistory (data) {
  const newhistory = [];
  const nosql = new FlyJson();
  const newdata = nosql.set(data).groupDetail('group_date').exec();
  try {
    if (newdata.length > 0) {
      const result = Object.entries(newdata[0].group_date);
      for (let i = 0; i < result[0].length; i++) {
        if (result[i] !== undefined) {
          for (let x = 0; x < result[i][1].length; x++) {
            if (x === 0) {
              const nhdate = new Date(result[i][1][x].date_created);
              newhistory.push({
                room_id: result[i][1][x].room_id,
                account_id: result[i][1][x].akun_id,
                account_type_id: 5,
                message_type_id: 1,
                message: result[i][0],
                message_id: result[i][1][x].message_id + '1',
                date_created: helper.formatDate(nhdate, true),
                date_created_unix: nhdate.getTime(),
                message_status_id: result[i][1][x].message_status_id,
                status_active_id: result[i][1][x].status_active_id,
                date_modified: result[i][1][x].date_modified
              });
            }
            newhistory.push(result[i][1][x]);
          }
        }
      }
    }
  } catch (error) {
    console.log('_manipulationHistory: ' + error);
  }
  return newhistory;
}

/**
 * Broadcast to all client (inside room only) if there is new user joined to the chat room
 * @param {object} data
 * @param {WebSocket} socket
 * @return {callback} emit to join event
 */
function joinRoom (data, socket) {
  data.date = helper.timestamp(new Date());
  data.message = 'is join to this chat.';
  socket.broadcast.to(data.room_id).emit('join',
    helper.sendResponse('true', 'Berhasil join to chatroom', '', data));
}

/**
 * Load History Messages
 * @param {string} roomId
 * @param {WebSocket} socket
 * @return {callback} emit to loadHistory event
 */
function loadMessages (roomId, socket) {
  return new Promise((resolve, reject) => {
    try {
      DBMS().query('select * from chat_message where room_id=$1 and status_active_id=$2', [roomId, 1]).promise().then(res => {
        return resolve(helper.sendResponse('true', 'History chat ' + (res.length > 0 ? '' : 'tidak ') + 'ditemukan', '', _manipulationHistory(_addGroupDate(res))));
      }).catch(err => {
        return reject(helper.sendResponse('false', 'Ada kesalahan...', 'error', err));
      });
    } catch (err) {
      return reject(helper.sendResponse('false', 'Ada kesalahan...', 'error', err));
    }
  });
}

/**
 * Insert Messages to database
 * @param {object} data
 * @return {string}
 */
function insertMessage (data) {
  return new Promise((resolve, reject) => {
    try {
      // insert message
      DBMS().query('insert into chat_message ' +
      '(message_id, room_id, account_id, account_type_id, message_type_id, message_status_id, message, date_created, date_created_unix, status_active_id) ' +
      'values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);',
      [data.message_id, data.room_id, data.account_id, data.account_type_id, data.message_type_id, data.message_status_id, data.message,
        data.date_created, data.date_created_unix, 1]).promise().then(res => {
        return resolve(helper.sendResponse('true', 'Pesan masuk', '', data));
      }).catch(err => {
        return reject(helper.sendResponse('false', 'Ada kesalahan...', 'error', err));
      });
    } catch (err) {
      return reject(helper.sendResponse('false', 'Ada kesalahan...', 'error', err));
    }
  });
}

/**
  * Broadcast Message to all client except sender
  * @param {object} data
  * @param {WebSocket} socket
  * @return {callback} emit to broadcast event
  */
function broadcastMessage (data, socket) {
  socket.broadcast.emit('broadcast', helper.sendResponse('true', 'Ada pesan broadcast baru', '', data));
}

/**
  * Broadcast event message is typing to all client in same room
  * @param {object} data
  * @param {WebSocket} socket
  * @return {callback} emit to typing event
  */
function messageIsTyping (data, socket) {
  socket.broadcast.to(data.room_id).emit('typing', helper.sendResponse('true', 'Sedang mengetik...', '', data));
}

/**
  * Broadcast event message is read to all client in same room
  * @param {object} data
  * @param {WebSocket} socket
  * @return {callback} emit to read event
  */
function messageIsRead (data) {
  return new Promise((resolve, reject) => {
    try {
      const date = new Date();
      const dateModified = helper.timestamp(date);
      // console.log('messageIsRead: ' + JSON.stringify(data));
      DBMS().query('update chat_message set message_status_id=$1, date_modified=$2, date_modified_unix=$3 where message_id=$4;', [3, dateModified, date.getTime(), data.message_id]).promise().then(res => {
        return resolve(helper.sendResponse('true', 'Status pesan telah read', '', data));
      }).catch(err => {
        return reject(helper.sendResponse('false', 'Ada kesalahan... ', 'error', err));
      });
    } catch (err) {
      return reject(helper.sendResponse('false', 'Ada kesalahan... ', 'error', err));
    }
  });
}

/**
  * Broadcast event message is deleted to all client in same room
  * @param {object} data
  * @param {WebSocket} socket
  * @return {callback} emit to delete event
  */
function messageIsDeleted (data) {
  return new Promise((resolve, reject) => {
    try {
      const date = new Date();
      const dateModified = helper.timestamp(date);
      // console.log('messageIsDeleted: ' + JSON.stringify(data));
      DBMS().query('update chat_message set status_active_id=$1, date_modified=$2, date_modified_unix=$3 where message_id=$4 and account_id=$5 and status_active_id <> $6', [2, dateModified, date.getTime(), data.message_id, data.account_id, 2]).promise().then(res => {
        return resolve(helper.sendResponse('true', 'Pesan ini telah dihapus', '', data));
      }).catch(err => {
        return reject(helper.sendResponse('false', 'Ada kesalahan... ', 'error', err));
      });
    } catch (err) {
      return reject(helper.sendResponse('false', 'Ada kesalahan... ', 'error', err));
    }
  });
}

module.exports = {
  hasKey: function (data, keyname) {
    return helper.hasKey(data, keyname);
  },
  sendResponse: function (stsres, stsdes, stsfal, datanya) {
    return helper.sendResponse(stsres, stsdes, stsfal, datanya);
  },
  messageSchema: schema.messageSchema,
  joinSchema: schema.joinSchema,
  readSchema: schema.readSchema,
  deleteSchema: schema.deleteSchema,
  typingSchema: schema.typingSchema,
  loadhistorySchema: schema.loadhistorySchema,
  joinRoom,
  insertMessage,
  loadMessages,
  broadcastMessage,
  messageIsTyping,
  messageIsRead,
  messageIsDeleted
};
