/* global F */
'use strict';

const fs = require('fs');

/**
  * Format to indonesian date (dd-mm-yyyy) or timestamp
  * @param {string} date
  * @param {bool} isDateTime
  * @return {string}
  */
function formatDate (date, isDateTime) {
  isDateTime = (isDateTime === undefined) ? false : isDateTime;
  const d = new Date(date);
  let month = '' + (d.getMonth() + 1);
  let day = '' + d.getDate();
  const year = d.getFullYear();
  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  if (isDateTime) {
    // hours as 2 digits (hh)
    const hours = ('0' + d.getHours()).slice(-2);
    // minutes as 2 digits (mm)
    const minutes = ('0' + d.getMinutes()).slice(-2);
    // seconds as 2 digits (ss)
    const seconds = ('0' + d.getSeconds()).slice(-2);
    return [year, month, day].join('-') + ' ' + hours + ':' + minutes + ':' + seconds;
  }
  return [day, month, year].join('-');
}

/**
 * Determine in object has key
 * @param {object} data
 * @param {string|array} keyname
 * @return {bool}
 */
function hasKey (data, keyname) {
  keyname = keyname instanceof Array ? keyname : [keyname];
  return keyname.every(key => Object.keys(data).includes(key));
}

/**
 * Convert base64 String to File
 * @param {string} dataString
 * @param {string} filename
 * @returns {string}
 */
function base64FileHandler (dataString, filename) {
  const matches = dataString.split(';base64,');

  if ((matches == null) || (matches instanceof String)) return '';
  const ext = matches[0].split('/');
  const file = filename + '.' + ext[1];
  const date = new Date();
  let subdir = 'data/files/' + date.format('yyyy') + '/' + date.format('MM') + '/' + date.format('dd') + '/';
  switch (true) {
    case (matches[0].toString().toLowerCase().includes('image')):
      subdir = 'data/images/' + date.format('yyyy') + '/' + date.format('MM') + '/' + date.format('dd') + '/';
      break;
    default:
      //
  }

  const dir = F.path.public() + subdir;
  F.path.mkdir(dir);

  const hostdir = F.config.ws_upload_base + '/' + subdir + file;
  const input = dir + file;

  fs.writeFile(input, Buffer.from(matches[1], 'base64'), function (err) {
    if (err) console.log(err);
  });
  return hostdir;
}

/**
 * Get date with timestamp format
 * @param {Date} date
 * @returns {string}
 */
function timestamp (date) {
  return date.toISOString().replace('T', ' ').replace('Z', '').substr(0, 19);
}

/**
 * Send response
 * @param {string} stsres
 * @param {string} stsdes
 * @param {string} stsfal
 * @param {*} datanya
 * @returns {object}
 */
function sendResponse (stsres, stsdes, stsfal, datanya) {
  const obj = {
    sts_res: stsres,
    sts_des: stsdes,
    sts_fal: stsfal
  };
  const newdata = {};
  if (stsfal === '' || stsfal === undefined || stsfal === null) {
    newdata.data = datanya;
  } else {
    if (datanya !== '' || datanya !== undefined || stsfal !== null) {
      newdata.error = datanya;
    }
  }
  Object.assign(obj, newdata);
  return obj;
}

module.exports = {
  formatDate,
  hasKey,
  base64FileHandler,
  timestamp,
  sendResponse
};
