/* global ErrorBuilder */

'use strict';

/**
 * Response success
 * @param {controller} $        this is the totaljs controller
 * @param {string} message      this is the message of response
 * @param {object} response     this is the output response
 * @return {callback}
 */
function success ($, message, response = {}) {
  let self = $;
  if ($.controller && $.controller.headers) self = $.controller;
  self.status = 200;
  const newdata = {
    status: 'success'
  };
  Object.assign(newdata, response);
  return self.json(_defaultMsg(message, 200, newdata));
}

/**
 * Response error
 * @param {controller} $        this is the totaljs controller
 * @param {object} error        this is the error object
 * @param {string} message      this is the message of response
 * @param {object} response     this is the output response
 * @return {callback}
 */
function error ($, error, message = 'Something went wrong!', response = {}) {
  let self = $;
  if ($.controller && $.controller.headers) self = $.controller;
  self.status = 400;
  const newdata = {
    status: 'error',
    error: error
  };
  Object.assign(newdata, response);
  return self.json(_defaultMsg(message, 400, newdata));
}

/**
 * Response custom
 * @param {controller} $        this is the totaljs controller
 * @param {string} message      this is the message of response
 * @param {string} status       this is the status of response
 * @param {object} response     this is the output response
 * @return {callback}
 */
function custom ($, code, message, status, response = {}) {
  let self = $;
  if ($.controller && $.controller.headers) self = $.controller;
  self.status = code;
  const newdata = {
    status: status
  };
  Object.assign(newdata, response);
  return self.json(_defaultMsg(message, code, newdata));
}

/**
 * Response bad request
 * @param {controller} $        this is the totaljs controller
 * @param {string} message      this is the message of response
 * @param {object} response     this is the output response
 * @return {callback}
 */
function badRequest ($, message, response = {}) {
  let self = $;
  if ($.controller && $.controller.headers) self = $.controller;
  self.status = 400;
  const newdata = {
    status: 'error',
    error: 'Bad Request'
  };
  Object.assign(newdata, response);
  return self.json(_defaultMsg(message, 400, newdata));
}

/**
 * Response bad unauthorized
 * @param {controller} $        this is the totaljs controller
 * @param {string} message      this is the message of response
 * @param {object} response     this is the output response
 * @return {callback}
 */
function unauthorized ($, message, response = {}) {
  let self = $;
  if ($.controller && $.controller.headers) self = $.controller;
  self.status = 401;
  const newdata = {
    status: 'error',
    error: 'Unauthorized'
  };
  Object.assign(newdata, response);
  return self.json(_defaultMsg(message, 401, newdata));
}

/**
 * Response forbidden
 * @param {controller} $        this is the totaljs controller
 * @param {string} message      this is the message of response
 * @param {object} response     this is the output response
 * @return {callback}
 */
function forbidden ($, message, response = {}) {
  let self = $;
  if ($.controller && $.controller.headers) self = $.controller;
  self.status = 403;
  const newdata = {
    status: 'error',
    error: 'Forbidden'
  };
  Object.assign(newdata, response);
  return self.json(_defaultMsg(message, 403, newdata));
}

/**
 * Response not found (404)
 * @param {controller} $        this is the totaljs controller
 * @param {string} message      this is the message of response
 * @param {object} response     this is the output response
 * @return {callback}
 */
function notFound ($, message, response = {}) {
  let self = $;
  if ($.controller && $.controller.headers) self = $.controller;
  self.status = 404;
  const newdata = {
    status: 'error',
    error: 'Not Found'
  };
  Object.assign(newdata, response);
  return self.json(_defaultMsg(message, 404, newdata));
}

/**
 * Response timeout (408)
 * @param {controller} $        this is the totaljs controller
 * @param {string} message      this is the message of response
 * @param {object} response     this is the output response
 * @return {callback}
 */
function timeout ($, message, response = {}) {
  let self = $;
  if ($.controller && $.controller.headers) self = $.controller;
  self.status = 408;
  const newdata = {
    status: 'error',
    error: 'Request Timeout'
  };
  Object.assign(newdata, response);
  return self.json(_defaultMsg(message, 408, newdata));
}

/**
 * Response conflict (409)
 * @param {controller} $        this is the totaljs controller
 * @param {string} message      this is the message of response
 * @param {object} response     this is the output response
 * @return {callback}
 */
function conflict ($, message, response = {}) {
  let self = $;
  if ($.controller && $.controller.headers) self = $.controller;
  self.status = 409;
  const newdata = {
    status: 'error',
    error: 'Request Conflict'
  };
  Object.assign(newdata, response);
  return self.json(_defaultMsg(message, 409, newdata));
}

/**
 * Modify error from schema
 * @param {string} name     this is the error key name
 */
function schemaErrorBuilder (name) {
  return ErrorBuilder.addTransform(name, function (isResponse) {
    const builder = [];

    for (let i = 0, length = this.items.length; i < length; i++) {
      const err = this.items[i];
      builder.push({ name: err.name, error: err.error });
    }

    if (isResponse) {
      this.status = 400;
      if (builder.length > 0) {
        return JSON.stringify({
          code: 400,
          status: 'error',
          message: 'Some field is invalid!',
          error: builder
        });
      } else {
        return JSON.stringify({
          code: 400,
          status: 'error',
          message: 'Something went wrong!',
          error: builder
        });
      }
    }
    return builder;
  });
}

/**
 * Default structure message
 * @param {string} message    this is the message of response
 * @param {integer} code      this is the code of http response
 * @param {object} obj        this is the output response detail
 * @returns {object}
 */
function _defaultMsg (message, code, obj) {
  const data = {
    code: code,
    message: message
  };
  Object.assign(data, obj);
  return data;
}

module.exports = {
  success,
  error,
  custom,
  badRequest,
  unauthorized,
  forbidden,
  notFound,
  timeout,
  conflict,
  schemaErrorBuilder
};
