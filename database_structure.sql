/*
 Navicat Premium Data Transfer

 Source Server         : PostreSQL GetMedik Local
 Source Server Type    : PostgreSQL
 Source Server Version : 130004
 Source Host           : 192.168.88.25:5432
 Source Catalog        : DB_CUG_CHAT
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130004
 File Encoding         : 65001

 Date: 17/09/2021 22:06:57
*/


-- ----------------------------
-- Table structure for chat_message
-- ----------------------------
DROP TABLE IF EXISTS "public"."chat_message";
CREATE TABLE "public"."chat_message" (
  "message_id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL,
  "room_id" varchar(36) COLLATE "pg_catalog"."default",
  "account_id" varchar(36) COLLATE "pg_catalog"."default",
  "account_type_id" int2,
  "message_type_id" int2,
  "message_status_id" int2,
  "message" text COLLATE "pg_catalog"."default",
  "date_created" timestamp(0),
  "date_created_unix" int8,
  "date_modified" timestamp(0),
  "date_modified_unix" int8,
  "status_active_id" int2
)
;

-- ----------------------------
-- Records of chat_message
-- ----------------------------

-- ----------------------------
-- Table structure for num_account_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."num_account_type";
CREATE TABLE "public"."num_account_type" (
  "account_type_id" int2 NOT NULL,
  "account_type" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of num_account_type
-- ----------------------------
INSERT INTO "public"."num_account_type" VALUES (1, 'Patient');
INSERT INTO "public"."num_account_type" VALUES (2, 'Doctor');
INSERT INTO "public"."num_account_type" VALUES (3, 'Channel');

-- ----------------------------
-- Table structure for num_status_active
-- ----------------------------
DROP TABLE IF EXISTS "public"."num_status_active";
CREATE TABLE "public"."num_status_active" (
  "status_active_id" int2 NOT NULL,
  "status_active" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of num_status_active
-- ----------------------------
INSERT INTO "public"."num_status_active" VALUES (1, 'Active');
INSERT INTO "public"."num_status_active" VALUES (2, 'Non Active');
INSERT INTO "public"."num_status_active" VALUES (3, 'Blocked by System');
INSERT INTO "public"."num_status_active" VALUES (4, 'Deleted by User');
INSERT INTO "public"."num_status_active" VALUES (5, 'Deleted by System');

-- ----------------------------
-- Primary Key structure for table chat_message
-- ----------------------------
ALTER TABLE "public"."chat_message" ADD CONSTRAINT "chat_message_pkey" PRIMARY KEY ("message_id");

-- ----------------------------
-- Primary Key structure for table num_account_type
-- ----------------------------
ALTER TABLE "public"."num_account_type" ADD CONSTRAINT "num_account_type_pkey" PRIMARY KEY ("account_type_id");

-- ----------------------------
-- Primary Key structure for table num_status_active
-- ----------------------------
ALTER TABLE "public"."num_status_active" ADD CONSTRAINT "num_status_active_pkey" PRIMARY KEY ("status_active_id");
